var express = require("express");

var app = express();

app.get("/", function( req, res ){      // Route, callback: This func tells what to do when a "get" request at the given route is called 
    res.send("Hello Expressjs");        // This func takes an obj as input & it sends this to the requesting client ("Hello Expressjs")
});


app.get('/headers', function(req, res){ 
    res.send(JSON.stringify(req.headers)); 
});


app.listen(8888);

console.log("Server is running | Express");
